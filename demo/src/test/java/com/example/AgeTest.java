package com.example;

import static org.junit.Assert.*;

import org.joda.time.LocalDate;
import org.junit.Test;

public class AgeTest 
{
    @Test
    public void newBornIsNotAdult()
    {
        LocalDate birthDateNow = new LocalDate();
        boolean result = Age.isAdult(birthDateNow);

        assertFalse(result);
    }

    @Test
    public void josfIsNotAdult()
    {
        LocalDate birthDate = new LocalDate(2021, 11, 3);
        boolean result = Age.isAdult(birthDate);

        assertFalse(result);
    }
    
    @Test
    public void ronaldIsAdult()
    {
        LocalDate birthDate = new LocalDate(1987, 12, 22);
        boolean result = Age.isAdult(birthDate);

        assertTrue(result);
    }

    @Test
    public void ronaldIsNow36()
    {
        LocalDate birthDate = new LocalDate(1987, 12, 22);
        int result = Age.calculateAge(birthDate);

        assertEquals(result, 36);
    }

    @Test
    public void exactlyOneYear() {
        LocalDate birthDate = new LocalDate().minusYears(1);
        int result = Age.calculateAge(birthDate);  
        
        assertEquals(result, 1);
    }

    @Test
    public void oneDayTillFirstBirthDay() {
        LocalDate birthDate = new LocalDate().minusYears(1).plusDays(1);
        int result = Age.calculateAge(birthDate);  
        
        assertEquals(result, 0);
    }
}
